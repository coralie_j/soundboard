import { View, Text, Pressable, StyleSheet, ScrollView, FlatList, Button } from "react-native";
import { useSelector } from "react-redux";
import { Audio } from 'expo-av';
import { useNavigation } from "@react-navigation/native";
import { SampleSelector } from './soundsSlice';
import { useState } from "react";


const TouchPadScreen = () =>{

    const navigation = useNavigation();
    const samples = useSelector(SampleSelector);
    let sound = new Audio.Sound();
    // let [sound, setSound ] = useState(new Audio.Sound());
    

    const playMusic = (item) => {
        try {

            /**
             *  Ici, j'initialise un object Sound puis je vérifie le type du son, si il est pré-enregistré
             *  ou non pour savoir comment déclarer le lien du son. Si le son a été rogné, je me sers de
             *  la fonction setOnPlaybackStatusUpdate qui est appelée à chaque fois qu'il y a une mise à jour
             *  sur le son/ sa lecture. Je vérifie que la position actuelle de la lecture n'est pas encore atteint
             *  à la fin du rognage. Si elle l'atteint, j'arrête la lecture du son et je pense bien à mettre à jour
             *  la fonction setOnPlaybackStatusUpdate du son à null sinon la fonction continuera de s'éxécuter tant que
             *  la durée totale du son n'est pas atteinte. Pour finir, je lance la méthode unload pour fermer le flux du son. 
             */

            // let sound = new Audio.Sound();

            if (item.type == "pre-registred")
                sound.loadAsync(item.lien, { shouldPlay: true, positionMillis: item.start ? item.start : 0 });
            else
                sound.loadAsync({ uri: item.lien}, { shouldPlay: true, positionMillis: item.start ? item.start : 0 });
            
            if (item.end){
                sound.setOnPlaybackStatusUpdate((playbackStatus) => {
                    if (playbackStatus.positionMillis >= item.end) {
                        console.log("Fin à " + playbackStatus.positionMillis);
                        sound.stopAsync();
                        sound.setOnPlaybackStatusUpdate(null);
                    }
                });
            }
            sound.unloadAsync();
            
            console.log(item);
        } catch (error){
            console.error(error);
        }
    };

    const defineColorPad = (item) => {

        // Ici, on calcule les couleurs des pads, il y a colonnes de pads
        // Donc chaque pad aura un index correspondant à la formule suivante : 4*colonne + index
        // où colonne correspond à la colonne où le pad sera   

        let index = samples.indexOf(item);
        let color;

        if (index % 4 == 0)
            color = "#26C9E7";
        else if (index %4 == 1)
            color = "#26E760";
        else if (index % 4 == 2)
            color = "red";
        else
            color = "yellow";
        return color;
    }

    const stopMusic = async() => {
        let status = await sound.getStatusAsync();
        if(status.isPlaying)
            sound.stopAsync();
    }

    return (
        <View style={styles.container}>
            <FlatList
                contentContainerStyle={{ width: "100%", justifyContent:'center', marginBottom: -1000 }}
                numColumns={4}
                data={samples}
                renderItem={({ item }) => (
                    <Pressable key={item.id}
                        style={{ borderColor: defineColorPad(item), borderWidth: 2, width: '21%', marginLeft: 10, height: 55, marginBottom: 10, borderRadius: 10 }}
                        onPress={() => playMusic(item)}
                        onLongPress={() => navigation.navigate('Modify', { id: item.id })}
                    >
                        <Text style={{ width: '100%' }}></Text>
                    </Pressable>
                )}
                keyExtractor={(item) => item.id}
            />
            <Pressable onPress={() => stopMusic()} style={{ backgroundColor: "#7A2442", padding: 12, borderRadius: 10 }}>
                <Text style={{ color: "#fff"}}>Stop the music </Text>
            </Pressable>
        </View>
    );

};

const styles = StyleSheet.create({
    container: {
        height: "90%",
        alignItems: 'center', 
        width: "100%",
        marginTop: 20, 
        paddingTop:10,
        justifyContent: 'center',
    },
});


export default TouchPadScreen;