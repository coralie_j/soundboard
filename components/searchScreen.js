import { StatusBar } from 'expo-status-bar';
import { useEffect, useState } from 'react';
import { StyleSheet, Text, View, Modal, TextInput, FlatList, Pressable } from 'react-native';
import Ionicons from "react-native-vector-icons/Ionicons";
import { addSample, SampleSelector } from './soundsSlice';
import { useDispatch, useSelector } from "react-redux";
import * as FileSystem from 'expo-file-system';
import Toast from 'react-native-root-toast';

const SearchScreen = () => {

    const [recherche, setRecherche] = useState('');
    const [resultats, setResultats] = useState([]);
    const [message, setMessage] = useState("");
    const [valideRecherche, setValidate] = useState(false);
    const dispatch = useDispatch();
    const samples = useSelector(SampleSelector);
    const soundboardDir = FileSystem.documentDirectory + 'soundboard/';

    const baseURL = "https://freesound.org/apiv2/search/text/";
    const baseURLUniqueSound = "https://freesound.org/apiv2/sounds/";
    const token = "yo4WKehcthdqXTzqWbi65wJKy84871678U3Lo9xV";

    /*
        Renvoie les sons qui correspondent à la recherche puis il n'y a pas de
        résultats, un message sera affiché.
    */

    const fetchDatasSounds = async () => {
        let request = await fetch(`${baseURL}?query=${recherche}&token=${token}`);
        let json = await request.json();
        let matchingSounds = json["results"];
        if (matchingSounds.length == 0)
            setMessage("Aucun résultat");
        setResultats(matchingSounds);
    };

    const fetchSoundData = async (id) => {
        let request = await fetch(`${baseURLUniqueSound}${id}/?token=${token}`);
        let sound = await request.json();
        let toast = Toast.show('Le téléchargement de votre son est en cours');

        // Ajout du son

        dispatch(addSample({
            titre: sound.name,
            description: sound.description,
            lien: sound.previews["preview-hq-mp3"],
            type: "freesound"
        }));

        const dirInfo = await FileSystem.getInfoAsync(soundboardDir);
        if (!dirInfo.exists) {
            console.log("Le répertoire 'soundboard' n'existe pas, on va le créer");
            await FileSystem.makeDirectoryAsync(FileSystem.documentDirectory + "soundboard");
        }

        let last_item = samples[samples.length - 1];

        // Téléchargement du son dans le répertoire de l'application

        let telechargement = await FileSystem.downloadAsync(
            sound.previews["preview-hq-mp3"],
            soundboardDir + 'sound-' + last_item.id + '.mp3'
        );

        telechargement.status == 200 ? console.log('Téléchargement terminé à ', telechargement.uri) : console.error("Une erreur s'est produite");
        Toast.hide(toast);
        Toast.show('Le téléchargement est terminée');
    }

    useEffect(() => {
        if (recherche.trim() != "")
            setValidate(true);
        else
            setValidate(false);
    }, [recherche]);

    return (
        <View style={styles.container}>
            <TextInput value={recherche} onChangeText={setRecherche} placeholder="Recherche ..." placeholderTextColor="white" style={{ padding: 15, color: "white" }}/>
            <Pressable
                style={{ backgroundColor: "#7A2442", opacity: valideRecherche ? 1 : 0.5, padding: 12, borderRadius: 10, alignItems: "center", width: "70%", marginBottom: 10, marginTop: 15 }}
                onPress={() => fetchDatasSounds()}
                disabled={!valideRecherche}
            >
                <Text style={{ color: "white", textAlign: "center" }}>Rechercher</Text>
            </Pressable>
            <StatusBar style="auto" />

            <Text>{message}</Text>

            <FlatList style={{ marginBottom: 10, marginTop: 20 }}
                data={resultats}
                renderItem={({ item }) => (
                    <View style={{ marginLeft: 10, flexDirection: 'row', alignItems: "center", justifyContent: "space-between", height: 50, marginTop: 10 }}>
                            <Text style={{color: "white"}}>{item.name}</Text>
                            <Pressable onPress={() => fetchSoundData(item.id)}>
                                <Ionicons name="add-circle-outline" color="#E63462" size={40} />
                            </Pressable>
                    </View>
                )}
                keyExtractor={(item) => item.id}
            />
        </View>
    );


}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
    },
    centeredView: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: 22,
    },
    modalView: {
        backgroundColor: 'white',
        borderRadius: 20,
        padding: 35,
        alignItems: 'center',
        shadowColor: '#000',
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 4,
        elevation: 5,
    },
    modalText: {
        marginBottom: 15,
        textAlign: 'center',
    }
});

export default SearchScreen;
