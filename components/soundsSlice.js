import { createSlice } from "@reduxjs/toolkit";
import uuid from "react-uuid";

const soundSlice = createSlice({
    name: "samples",
    initialState: [
        { id: 1, titre: 'clap_1.wav', lien: require('../assets/sounds/clap_1.wav'), type: 'pre-registred'},
        { id: 2, titre: 'clap_2.wav', lien: require('../assets/sounds/clap_2.wav'), type: 'pre-registred'},
        { id: 3, titre: 'fx_1.wav', lien: require('../assets/sounds/fx_1.wav'), type: 'pre-registred'},
        { id: 4, titre: 'fx_2.wav', lien: require('../assets/sounds/fx_2.wav'), type: 'pre-registred'},
        { id: 5, titre: 'kick_2.wav', lien: require('../assets/sounds/kick_2.wav'), type: 'pre-registred'},
        { id: 6, titre: 'kick_1.wav', lien: require('../assets/sounds/kick_1.wav'), type: 'pre-registred'},
        { id: 7, titre: 'shaker_1.wav', lien: require('../assets/sounds/shaker_1.wav'), type: 'pre-registred'},
        { id: 8, titre: 'shaker_2.wav', lien: require('../assets/sounds/shaker_2.wav'), type: 'pre-registred'},
        { id: 9, titre: 'shaker_3.wav', lien: require('../assets/sounds/shaker_3.wav'), type: 'pre-registred'},
        { id: 10, titre: 'snare_1.wav', lien: require('../assets/sounds/snare_1.wav'), type: 'pre-registred'},
        { id: 11, titre: 'snare_2.wav', lien: require('../assets/sounds/snare_2.wav'), type: 'pre-registred'},
        { id: 12, titre: 'tom_1.wav', lien: require('../assets/sounds/tom_1.wav'), type: 'pre-registred'},
        { id: 13, titre: 'tom_2.wav', lien: require('../assets/sounds/tom_2.wav'), type: 'pre-registred'},
        { id: 14, titre: 'tom_3.wav', lien: require('../assets/sounds/tom_3.wav'), type: 'pre-registred'},
        { id: 15, titre: 'tom_4.wav', lien: require('../assets/sounds/tom_4.wav'), type: 'pre-registred'},
        { id: 16, titre: 'adele.mp3', lien: require('../assets/sounds/omg.mp3'), type: 'pre-registred' }
    ],
    reducers: {
        addSample: (state, action ) => {
            return [...state, {
                id: uuid(),
                titre: action.payload.titre,
                description: action.payload.description,
                lien: action.payload.lien,
                type: action.payload.type
            }];
        },
        removeSample: (state, action) => {
            return state.filter((item) => item.id != action.payload);
        },
        addTrim: (state, action) => {
            return state.map((item) => item.id == action.payload.id ? { ...item, start: action.payload.leftTrim, end: action.payload.rightTrim }: item
            );
        }
    }
});

export const { addSample, removeSample, addTrim } = soundSlice.actions;
export const SampleSelector = (state) => state.samples;
export const PreregistredSamples = (state) => state.samples.filter((item) => item.type == "pre-registred");
export default soundSlice.reducer;
