import { View, Pressable, Text } from "react-native";
import Ionicons from "react-native-vector-icons/Ionicons";

import { SampleSelector, addTrim } from './soundsSlice';
import { useSelector, useDispatch } from "react-redux";
import { useRoute } from "@react-navigation/native";
import { useNavigation } from "@react-navigation/native";
import Toast from "react-native-root-toast";

import Trimmer from 'react-native-trimmer';
import { useState } from 'react';
import { Audio } from 'expo-av';


const ModifyScreen = () => {

    let samples = useSelector(SampleSelector);
    let item;
    const route = useRoute();
    const navigation = useNavigation();
    const dispatch = useDispatch();

    const [leftHandlePosition, setLeftPosition] = useState(0);
    const [rightHandlePosition, setRightPosition] = useState(100);
    const [duration, setDuration] = useState(500);

    const selectItem = (liste, id) => {
        for (let track of liste) {
            if (track.id == id) {
                return track;
            }
        }
    };

    // Fonction de calcul de la durée du son

    const getDuration = async (item) => {
        let sound;

        if (item.type == "pre-registred") {
            sound = await Audio.Sound.createAsync(
                item.lien,
                { shouldPlay: false }
            );
        } else {
            sound = await Audio.Sound.createAsync(
                { uri: item.lien },
                { shouldPlay: false }
            );
        }
        
        setDuration(sound.status.durationMillis);
    }

    if (route.params.id != null) {
        item = selectItem(samples, route.params.id);
        getDuration(item);
    }

    const setTrimmers = ({ leftPosition, rightPosition }) => {
        setLeftPosition(leftPosition);
        setRightPosition(rightPosition);
    }

    // Fonction de rognage du son

    const trimSound = () => {
        dispatch(addTrim({
            id: route.params.id,
            leftTrim: leftHandlePosition,
            rightTrim: rightHandlePosition
        }));
        Toast.show("Le son a été rogné");
    }


    return (
        <View style={{ marginTop: 20}}>
            <Trimmer
                totalDuration={duration}
                trimmerLeftHandlePosition={leftHandlePosition}
                trimmerRightHandlePosition={rightHandlePosition}
                onHandleChange={setTrimmers}
                trackBackgroundColor="#A3A5C3"
                markerColor="#7A2442"
                tintColor="#E63462"
            />
            <View style={{ justifyContent:"center", margin: 0, alignItems: "center", marginTop: 40}}>

                <Pressable 
                    style={{ borderColor: "#7A2442", borderWidth: 2, backgroundColor: "black", padding: 12, borderRadius: 10, alignItems: "center", width: "50%" }}
                    onPress={() => navigation.navigate('Change Sound')}
                >
                    <Text style={{color: "white"}}>Changer de son</Text>
                </Pressable>
                
                <Pressable style={{ backgroundColor: "#7A2442", padding: 7, borderRadius: 10, marginTop: 40, alignItems: "center", width: "50%" }} onPress={() => trimSound()}>
                    <Ionicons name="cut-outline" color="white" size={25} />
                </Pressable>
            </View>
        </View>
    );
    
}


export default ModifyScreen;