import { View, Text, Button, Pressable, FlatList, ScrollView } from "react-native";

import { PreregistredSamples, addSample } from './soundsSlice';
import { useSelector, useDispatch } from "react-redux";
import Ionicons from "react-native-vector-icons/Ionicons";
import { useNavigation } from "@react-navigation/native";

const ChangeSoundScreen = () => {

    const samples = useSelector(PreregistredSamples);
    const navigation = useNavigation();
    const dispatch = useDispatch();

    const addSound = (item) => {
        dispatch(addSample({
            titre: item.titre,
            description: item.description,
            lien: item.lien,
            type: "pre-registred"
        }));
    }

    return (
        <View style={{ marginTop: 20}}>
            <ScrollView nestedScrollEnabled={true}>
            <Text style={{fontSize: 20, fontWeight: 'bold', color: "#fff", marginBottom: 20}}> Nos samples pré-enregistrés</Text>
            <FlatList style={{ marginBottom: 10 }}
                data={samples}
                renderItem={({ item }) => (
                    <Pressable>
                        <View style={{ width: '95%', marginLeft: 10, flexDirection: 'row', alignItems: "center", justifyContent: "space-between", height: 50, marginTop: 10 }}>
                                <Text style={{color: "white"}}>{item.titre}</Text>
                                <Pressable onPress={() => addSound(item)}>
                                <Ionicons name="add-circle-outline" color="#E63462" size={40} />
                                </Pressable>
                        </View>
                    </Pressable>
                )}
                keyExtractor={(item) => item.id}
            />

            <View style={{ justifyContent: "center", margin: 0, alignItems: "center", marginTop: 40, marginBottom: 30 }}>

                <Pressable
                    style={{ borderColor: "#7A2442", borderWidth: 2, backgroundColor: "black", padding: 12, borderRadius: 10, alignItems: "center", width: "50%" }}
                    onPress={() => navigation.navigate('Freesound')}
                >
                    <Text style={{ color: "white" }}>Rechercher sur Freesound</Text>
                </Pressable>

                <Pressable style={{ backgroundColor: "#7A2442", padding: 7, borderRadius: 10, marginTop: 40, alignItems: "center", width: "50%" }} onPress={() => navigation.navigate("Microphone")}>
                    <Ionicons name="mic" color="white" size={25} />
                </Pressable>
            </View>
            </ScrollView>
        </View>
    );
};

export default ChangeSoundScreen;