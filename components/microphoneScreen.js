import { useState, useEffect } from 'react';
import { View, StyleSheet, Button, TextInput, Pressable, Text } from 'react-native';
import { Audio } from 'expo-av';
import { addSample, SampleSelector } from './soundsSlice';
import { useDispatch, useSelector } from "react-redux";
import * as FileSystem from 'expo-file-system';
import Toast from 'react-native-root-toast';

const MicrophoneScreen = () => {
    
    const [enregistrement, setRecording] = useState();
    const [description, setDescription ] = useState('');
    const [titre, setTitre] = useState('');

    // Uri où sera stocké le son du micro par défaut

    const [uri, setUri] = useState("");


    // Pour activer/désactiver le bouton pour sauvegarder le son 

    const [valideRecording, setValidate] = useState(false);
    
    const dispatch = useDispatch();
    const samples = useSelector(SampleSelector);

    // Répertoire où on va stocker le son

    const soundboardDir = FileSystem.documentDirectory + 'soundboard/';

    const startRecording = async() => {
        try {
            // Demande des permissions pour enregistrer la voix
            let permission = await Audio.requestPermissionsAsync();

            // Si on a les droits, on enregistre la voix

            if (permission.granted){
                const { recording } = await Audio.Recording.createAsync(
                    Audio.RECORDING_OPTIONS_PRESET_HIGH_QUALITY
                );
                setRecording(recording);
            } else {
                console.log("Droit insuffisant pour enregistrer une voix");
            }
            
        } catch (err) {
            console.error('Failed to start recording', err);
        }
    }

    const stopRecording = async () => {
        await enregistrement.stopAndUnloadAsync();
        setUri(enregistrement.getURI());
        setRecording(undefined);
        console.log('Recording saved at ', enregistrement.getURI());
    }

    const saveRecording = async() => {
        // On vérifie si le répertoire de stockage des sons existe ou pas

        const metaDataDir = await FileSystem.getInfoAsync(soundboardDir);
        let existe = metaDataDir.exists;
        let toast = Toast.show('Le téléchargement de votre son est en cours');

        // Si il n'existe pas, on le créé

        if (!existe) {
            console.log("Le répertoire 'soundboard' n'existe pas, on va le créer");
            try {
                await FileSystem.makeDirectoryAsync(FileSystem.documentDirectory + "soundboard");
            } catch (error) {
                console.log(error);
            }

        }

        /* Puisque l'enregistrement est stocké dans le cache de l'application, on a juste à 
           le déplacer dans le répertoire où on stocke les sons de l'application. Ensuite, on
           stocke les infos du son dans le store.
        */

        let last_item = samples[samples.length - 1];
        FileSystem.moveAsync({
            from: uri,
            to: soundboardDir + 'recording-' + last_item.id + '.mp3',
        });

        dispatch(addSample({
            titre: titre,
            description: description,
            lien: soundboardDir + 'recording-' + last_item.id + '.mp3',
            type: "recording"
        }));

        Toast.hide(toast);
        Toast.show('Le téléchargement est terminée');
    }

    useEffect(() => {
        if (titre.trim() != "" && description.trim() != '' && uri != '')
            setValidate(true);
        else
            setValidate(false);
    }, [titre, description, uri]);

    return (
        <View style={styles.container}>
            <Pressable 
                style={{ backgroundColor: "#7A2442", padding: 15, borderRadius: 10, alignItems: "center", width: "70%", marginBottom: 10 }}
                onPress={enregistrement ? () => stopRecording() : () => startRecording()}
            >
                <Text style={{ color: "white", textAlign: "center"}}>{enregistrement ? "Arrêter l'enregistrement" : "Commencer l'enregistrement"}</Text>
            </Pressable>
            <TextInput value={titre} onChangeText={setTitre} placeholder='Titre...' placeholderTextColor="white" style={{padding: 15, color: "white"}} />
            <TextInput value={description} onChangeText={setDescription} placeholder="Description..." placeholderTextColor="white" style={{ textAlign: "center", padding: 15, marginBottom: 40, color:"white" }}  />
            
            <Pressable
                style={{ borderColor: "#7A2442", borderWidth: 2, backgroundColor: "black", opacity:  valideRecording ? 1 : 0.5, padding: 12, borderRadius: 10, alignItems: "center", width: "50%" }}
                onPress={() => saveRecording()} disabled={!valideRecording}
            >
                <Text style={{ color: "white" }}>Sauvegarder</Text>
            </Pressable>
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: "center",
        padding: 10,
    },
});

export default MicrophoneScreen;
