import { configureStore, combineReducers } from "@reduxjs/toolkit";
import thunk from 'redux-thunk';
import { persistReducer } from 'redux-persist';
import sampleReducer from "./components/soundsSlice";
import AsyncStorage from '@react-native-async-storage/async-storage';

const reducers = combineReducers({
    samples: sampleReducer,
});

const persistConfig = {
    key: "root",
    storage: AsyncStorage,
};

const persistedReducer = persistReducer(persistConfig, reducers);

const store = configureStore({
    reducer: persistedReducer,
    middleware: [thunk],
});

export default store;