import { Provider } from "react-redux";
import store from "./store";
import TouchPadScreen from './components/touchPadScreen';
import ModifyScreen from './components/modifyScreen';
import ChangeSoundScreen from './components/changeSoundScreen';
import SearchScreen from './components/searchScreen';
import MicrophoneScreen from './components/microphoneScreen';

import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import { RootSiblingParent } from 'react-native-root-siblings';

import { persistStore } from "redux-persist";
import { PersistGate } from "redux-persist/integration/react";

const Stack = createNativeStackNavigator();
let persistor = persistStore(store);

const MyTheme = {
  dark: true,
  colors: {
    primary: 'rgb(255, 45, 85)',
    background: '#051923',
    card: 'rgb(255, 255, 255)',
    text: 'rgb(28, 28, 30)',
    border: 'rgb(199, 199, 204)',
    notification: 'rgb(255, 69, 58)',
  },
};

const styles = {
    container: {
        headerStyle: {
            backgroundColor: '#E63462',
        },
        headerTintColor: '#fff'
    },
};

export default function App() {
  return (
    <RootSiblingParent>
        <Provider store={store}>
            <PersistGate persistor={persistor} loading={null}>
                <NavigationContainer theme={MyTheme}>
                        <Stack.Navigator >
                            <Stack.Screen name="Pad" component={TouchPadScreen} options={styles.container} />
                            <Stack.Screen name='Modify' component={ModifyScreen} options={styles.container} />
                            <Stack.Screen name='Change Sound' component={ChangeSoundScreen} options={styles.container} />
                            <Stack.Screen name='Freesound' component={SearchScreen} options={styles.container} />
                            <Stack.Screen name='Microphone' component={MicrophoneScreen} options={styles.container} />
                        </Stack.Navigator>
                    </NavigationContainer>
            </PersistGate>
        </Provider>
    </RootSiblingParent>
  );
}


